package com.danilojakob.service;

import org.junit.Assert;
import org.junit.Test;

public class ServiceTest {
	
	@Test
	public void testClassVariables() {
		WeatherService service1 = new WeatherService();
		WeatherService service2 = new WeatherService();
		
		Assert.assertEquals("The two lists should be the same because they're class variables", service1.getStations(), service2.getStations());
	}

}
