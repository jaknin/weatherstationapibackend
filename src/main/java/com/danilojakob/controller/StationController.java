package com.danilojakob.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danilojakob.WeatherApiConstants;
import com.danilojakob.service.WeatherService;
import com.danilojakob.util.WeatherStation;

/**
 * StationController for getting current stations
 * @author jada
 *
 */
@RestController
public class StationController implements WeatherApiConstants {
	
	@CrossOrigin(origins = "*")
	@GetMapping(value=WEATHER_STATION_INFO_ENDPOINT, produces = "application/json")
	public List<WeatherStation> station(HttpServletResponse response) {
		WeatherService service = new WeatherService();
		return service.getStations();
	}
}
