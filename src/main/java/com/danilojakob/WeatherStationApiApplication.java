package com.danilojakob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherStationApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherStationApiApplication.class, args);
	}

}
