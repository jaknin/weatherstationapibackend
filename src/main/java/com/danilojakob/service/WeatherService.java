package com.danilojakob.service;

import java.util.ArrayList;
import java.util.List;

import com.danilojakob.WeatherApiConstants;
import com.danilojakob.data.StationData;
import com.danilojakob.util.WeatherStation;
import com.danilojakob.util.sensor.AirHumiditySensor;
import com.danilojakob.util.sensor.ISensor;
import com.danilojakob.util.sensor.TemperatureSensor;
import com.danilojakob.util.sensor.WindPaceSensor;

/**
 * Service for creating the weather stations
 * and retrieving data from them
 * @author jada
 *
 */
public class WeatherService implements WeatherApiConstants {
	
	//Locations of the weather stations
	private final String[] locations = {"Prag", "Zürich", "Lugano", "New York", "Tokyo", "Rio De Janeiro", "Adliswil", "Moskau", "Genf", "Kapstadt", "Los Angeles", "Sardegna", "Lissabon", "Bagdad", "Uster", "Wietzikon", "Vancouver", "Louisiana", "El Paso", "Landquart", "Maienfeld"};
	//Save already used locations
	private static List<String> selectedLocations = new ArrayList<>();
	//Class variables that need to be the same over all instances
	private static List<WeatherStation> stations = new ArrayList<>();
	private static int counter = 0;
	
	public WeatherService() {
		if (counter == 0) createStations();
		counter++;
	}
	
	/**
	 * Create 5 different weather stations
	 */
	private void createStations() {
		for (int i = 0; i < 9; i++) {
			
			ISensor temperaturSensor = new TemperatureSensor();
			ISensor airHumiditySensor = new AirHumiditySensor();
			ISensor paceSensor = new WindPaceSensor();
			//
			String location = "";
			boolean exists = false;
			do {
				int index = (int) (Math.random() * locations.length);
				location = locations[index];
				exists = checkExistence(location);
			} while (exists);
			selectedLocations.add(location);
			stations.add(new WeatherStation(temperaturSensor, paceSensor, airHumiditySensor, i + 1, location));
		}
	}
	
	/**
	 * Check if a location is already used
	 * @param location {@link String} to check
	 * @return If the location is already used
	 */
	private boolean checkExistence(String location) {
		for (int i = 0; i < selectedLocations.size(); i++) {
			if (location.equals(selectedLocations.get(i))) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Get the all the weather stations
	 * @return {@link List} of {@link WeatherStation}
	 */
	public List<WeatherStation> getStations() {
		return stations;
	}
	
	/**
	 * Get station data for a selected station
	 * @param id {@link Integer} stations id
	 * @return {@link StationData}
	 */
	public StationData getData(Integer id) {
		return stations.get(id).getStationData();
	}
}
