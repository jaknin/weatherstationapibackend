package com.danilojakob.data;

/**
 * Class for holding the different measurments 
 * from the stations
 * @author jada
 */
public class StationData {
	
	private double temperature;
	private double airHumidity;
	private double windPace;
	
	public StationData(double temperature, double airHumidity, double windPace) {
		this.temperature = temperature;
		this.airHumidity = airHumidity;
		this.windPace = windPace;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getAirHumidity() {
		return airHumidity;
	}

	public void setAirHumidity(double airHumidity) {
		this.airHumidity = airHumidity;
	}

	public double getWindPace() {
		return windPace;
	}

	public void setWindPace(double windPace) {
		this.windPace = windPace;
	}
	
	
}
