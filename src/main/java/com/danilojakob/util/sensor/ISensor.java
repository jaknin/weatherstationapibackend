package com.danilojakob.util.sensor;

/**
 * Interface for shared sensor methods
 * @author jada
 *
 */
public interface ISensor {
	
	/**
	 * Gets the current data from a sensor
	 * @return data
	 */
	public float getCurrentData();
	
	/**
	 * Changes the data from the sensor randomly
	 */
	public void randomChange();
}
