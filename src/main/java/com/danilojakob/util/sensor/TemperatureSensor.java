package com.danilojakob.util.sensor;

/**
 * TemperatureSensor class
 * @author jada
 *
 */
public class TemperatureSensor implements ISensor {

	private float maxTemperatur;

	private float minTemperatur;

	private float maxTemperatureChange;
	
	private float currentTemperature;

	/**
	 * Constructor which generates all the parameters
	 */
	public TemperatureSensor() {
		this((float) Math.random() * 20 + 10, (float) Math.random() * 10 - 5, 1);
	}
	
	/**
	 * Constructor for the {@link TemperatureSensor} class
	 * @param maxTemperatur float 
	 * @param minTemperatur float
	 * @param maxTemperatureChange float
	 */
	public TemperatureSensor(float maxTemperatur, float minTemperatur, float maxTemperatureChange) {
		this.maxTemperatur = maxTemperatur;
		this.minTemperatur = minTemperatur;
		this.maxTemperatureChange = maxTemperatureChange;
		currentTemperature = ((float) Math.random()) * (this.maxTemperatur - this.minTemperatur) + this.minTemperatur;
	}

	@Override
	public float getCurrentData() {
		randomChange();
		return currentTemperature;
	}
	
	@Override
	public void randomChange() {
		this.currentTemperature = this.currentTemperature + this.maxTemperatureChange * ((float) Math.random() - 1) * 2;
		if (currentTemperature > maxTemperatur) {
			currentTemperature = maxTemperatur;
		}
		if (currentTemperature < minTemperatur) {
			currentTemperature = minTemperatur;
		}	
	}
}
