package com.danilojakob.util.sensor;

/**
 * WindPaceSensor class
 * @author jada
 *
 */
public class WindPaceSensor implements ISensor {
	
	private float maxWindPace;
	
	private float minWindPace;
	
	private float currentWindPace;
	
	/**
	 * Constructor which generates the parameters
	 */
	public WindPaceSensor() {
		this((float) Math.random()*60+50,0);
	}
	
	/**
	 * Constructor for the {@link WindPaceSensor} class
	 * @param maxWindPace float
	 * @param minWindPace float
	 */
	public WindPaceSensor(float maxWindPace, float minWindPace) {
		
		this.maxWindPace = maxWindPace;
		this.minWindPace = minWindPace;
		currentWindPace = (float) Math.random() * (this.maxWindPace - this.minWindPace) + this.minWindPace;
		
	}
	
	@Override
	public float getCurrentData() {
		randomChange();
		return currentWindPace;
	}
	
	@Override
	public void randomChange() {
		this.currentWindPace = this.currentWindPace + ((float) Math.random() * 10 - 5) * 2;
		if (currentWindPace > maxWindPace) {
			currentWindPace = maxWindPace;
		}
		if (currentWindPace < minWindPace) {
			currentWindPace = minWindPace;
		}	
	}
	

}
