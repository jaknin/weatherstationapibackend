package com.danilojakob.util.sensor;

/**
 * AirHumidity sensor class
 * @author jada
 *
 */
public class AirHumiditySensor implements ISensor {

	private static final float MAXAIRHUMIDITY = 100f;

	private static final float MINAIRHUMIDITY = 0f;

	private float currentAirHumidity;
	
	public AirHumiditySensor() {
		currentAirHumidity = (float) Math.random() * (MAXAIRHUMIDITY - MINAIRHUMIDITY) + MINAIRHUMIDITY;
	}

	@Override
	public float getCurrentData() {
		randomChange();
		return currentAirHumidity;
	}
	
	@Override
	public void randomChange() {
		currentAirHumidity = (float) (Math.random() *5 - 2.5 ) + currentAirHumidity;
		
		if (currentAirHumidity > MAXAIRHUMIDITY) {
			currentAirHumidity = MAXAIRHUMIDITY;
		}
		
		if (currentAirHumidity < MINAIRHUMIDITY) {
			currentAirHumidity = MINAIRHUMIDITY;
		}		
	}
}
