package com.danilojakob.util;

import com.danilojakob.data.StationData;
import com.danilojakob.util.sensor.AirHumiditySensor;
import com.danilojakob.util.sensor.ISensor;
import com.danilojakob.util.sensor.TemperatureSensor;
import com.danilojakob.util.sensor.WindPaceSensor;

/**
 * Weather station class
 * @author jada
 *
 */
public class WeatherStation  {

	private ISensor temperaturSensor;

	private ISensor windPaceSensor;

	private ISensor airHumiditySensor;
	
	private int id;
	
	private String location;
	
	/**
	 * Constructor of the WeatherStation
	 * @param temperaturSensor {@link TemperatureSensor} of the station
	 * @param windPaceSensor {@link WindPaceSensor} of the station
	 * @param airHumiditySensor {@link AirHumiditySensor} of the station
	 * @param id Id of the station
	 * @param location {@link String} location of the station
	 */
	public WeatherStation(ISensor temperaturSensor, ISensor windPaceSensor, ISensor airHumiditySensor, int id, String location) {
		this.temperaturSensor = temperaturSensor;
		this.windPaceSensor = windPaceSensor;
		this.airHumiditySensor = airHumiditySensor;
		this.id = id;
		this.location = location;
	}
	
	/**
	 * Return all sensor values in a {@link StationData} class
	 * @return
	 */
	public StationData getStationData() {
		return new StationData(this.temperaturSensor.getCurrentData(), this.airHumiditySensor.getCurrentData(), this.windPaceSensor.getCurrentData());
	}
	
	/**
	 * Get id from current weather station
	 * @return
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * Get the location from current weather station
	 * @return {@link String}
	 */
	public String getLocation() {
		return this.location;
	}
}
