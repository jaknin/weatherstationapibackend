package com.danilojakob;

/**
 * Interface for constants
 * @author jada
 *
 */
public interface WeatherApiConstants {
	
	//End points
	final String WEATHER_STATION_ENDPOINT = "/station";
	final String WEATHER_STATION_INFO_ENDPOINT = "/info";
}
